import VARIABLES
import kivy
kivy.require('2.0.0')  # replace with your current kivy version !

from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button


class Texte(Label):
    def __init__(self, **kwargs):
        Label.__init__(self, **kwargs)
        self.bind(size=self.set_size)

    def set_size(self, widget, value):
        widget.text_size = self.size


class DisplayUserInput(GridLayout):
    def __init__(self, **kwargs):
        super(DisplayUserInput, self).__init__(**kwargs)
        self.cols = 1
        self.label2 = Texte(text='Saisissez un texte :')
        self.add_widget(self.label2)
        self.texte = TextInput(multiline=True)
        self.add_widget(self.texte)
        self.texte.bind(text=self.change_text)
        self.buttons_list = []
        self.grid = GridLayout()
        self.grid.cols = 3

        for item in VARIABLES.COLORS:
            button = Button(text=item)
            self.buttons_list.append(button)

        for item in self.buttons_list:
            self.grid.add_widget(item)
            item.bind(on_press=self.change_color)

        self.add_widget(self.grid)

    def change_text(self, widget, val):
        self.label2.text = str(widget.text)

    def change_color(self, widget):
        if widget.text == "Rouge":
            self.label2.color = 'red'
        elif widget.text == "Bleu":
            self.label2.color = 'blue'
        elif widget.text == "Vert":
            self.label2.color = 'green'
        elif widget.text == "Jaune":
            self.label2.color = 'yellow'
        elif widget.text == "Rose":
            self.label2.color = 'pink'
        elif widget.text == "Marron":
            self.label2.color = 'brown'


class MyApp(App):

    def build(self):
        return DisplayUserInput()


if __name__ == '__main__':
    MyApp().run()
